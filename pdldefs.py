import yaml
from itertools import chain

object_counter = 0

class PDLObject(yaml.YAMLObject):
    def __new__(cls):
        global object_counter
        object_counter += 1

        obj = super().__new__(cls)
        obj.pdlid = "{}-{}".format(cls.__name__, object_counter)

        print ("Created {}".format(obj.pdlid))
        return obj

    @staticmethod
    def get_pos(column, line):
        return f"pos-{column}-{line}"

    def __repr__(self):
        return f"""<{self.pdlid}>"""

class PDLStep(PDLObject):
    node_name_counter = 0
    
    def length(self):
        return 1

    def tikz_desc(self):
        return f"""% drawing node of type {self.__class__.__name__} in matrix line {self.line} with attributes: {self.__dict__!r}"""

    def tikz(self):
        return ""

    def tikz_arrows(self):
        return ""

    def tikz_above(self):
        if not self.text_above:
            return ""
        else:
            text = r"\contour{white}{%s}" % self.text_above
            print ("CONTOUR: " + text)
            return r"""node [pdl_arrow_text,above=2.6pt,anchor=base]{%s}""" % text

    def tikz_below(self):
        if not self.text_below:
            return ""
        else:
            text = r"\contour{white}{%s}" % self.text_below
            return r"""node [pdl_arrow_text,below=8pt,anchor=base]{%s}""" % text

    def get_column(self):
        return -1

    def create_affecting_node_name(self, parties_affected):
        name = f"node{id(self)}n{self.node_name_counter}"
        self.node_name_counter += 1
        for p in parties_affected:
            p.add_affecting_node(name)
        return name

    def init(self):
        pass
    
class MultiStep(PDLStep):
    def length(self):
        return self.length_fun(x.length() for x in self.steps)
    
    def draw(self):
        for d in self.steps:
            d.draw()

    def walk(self, start_line=0):
        line = start_line
        for step in self.steps:
            if isinstance(step, MultiStep):
                for l, substep in step.walk(line):
                    line = max(l, line)
                    yield l, substep
            else:
                yield line, step
            if self.increment == 1:
                line += self.increment
            else:
                line = start_line

class Parallel(MultiStep):
    yaml_tag = '!Parallel'
    length_fun = max
    increment = 0

class Serial(MultiStep):
    yaml_tag = '!Serial'
    length_fun = sum
    increment = 1

class Protocol(Serial):
    yaml_tag = '!Protocol'
    extra_steps = []

    def init(self):
        line_offset = 1 if self.has_groups else 0
        last_starts = {}
        for p, column in zip(self.parties, range(len(self.parties))):
            p.column = column
            
        for line, step in self.walk():
            step.line = line + line_offset
            step.init()
            if isinstance(step, StartParty):
                if step.party in last_starts:
                    raise Exception("Started party that was already started: " + step.party)
                last_starts[step.party] = step
            if isinstance(step, EndParty):
                if step.party not in last_starts:
                    raise Exception("Ended party that was not started: " + step.party)
                last_starts[step.party].end = step
                del last_starts[step.party]
        if len(last_starts):
            raise Exception("Party was started but not ended: " + repr(last_starts))

    @property
    def has_groups(self):
        return hasattr(self, 'groups') and self.groups is not None
        
        
class HTTPRequest(PDLStep):
    yaml_tag = '!http-request'
    method = ""
    url = ""
    parameters = ""
    type = "request"

    def init(self):
        self.text_above = " ".join((self.method, self.url, )).strip()
        self.text_below = self.parameters
    
    def tikz_arrows(self):
        f = getattr(self, 'from')
        t = getattr(self, 'to')
        from_ = self.get_pos(f.column, self.line)
        to_ = self.get_pos(t.column, self.line)
        return fr"""%% draw http {self.type}
        \draw[pdl_http_{self.type}] ({from_}) to {self.tikz_above()} {self.tikz_below()} ({to_});"""

    @property
    def height(self):
        if self.text_above and self.text_below:
            return "6ex", "center"
        elif self.text_above:
            return "3ex", "south"
        elif self.text_below:
            return "3ex", "north"
        else:
            return "1ex", "center"
    

class HTTPResponse(HTTPRequest):
    yaml_tag = '!http-response'
    code = ""
    headers = ""
    type = "response"

    def init(self):
        self.text_above = " ".join((self.code, self.headers, )).strip()
        self.text_below = self.parameters    

    def tikz_arrows(self):
        if getattr(self, 'for'):
            setattr(self, 'to', getattr(getattr(self, 'for'), 'from'))
            setattr(self, 'from', getattr(self, 'for').to)
        return super().tikz_arrows()
        

class Action(PDLStep):
    yaml_tag = '!action'

    def tikz(self):
        pos = self.get_pos(self.party.column, self.line)
        text = self.label
        self.node_name = self.create_affecting_node_name((self.party, ))
        out = fr"""\node[pdl_action,name={self.node_name}] at ({pos}) {{{text}}};"""
        return out

    def get_column(self):
        return self.party.column

    @property
    def height(self):
        return "3ex", "center"

    
class ScriptAction(Action):
    yaml_tag = '!script-action'
    data = ""
    label = ""

    def init(self):
        self.party = getattr(self, 'from')
    
    def tikz_arrows(self):
        f = getattr(self, 'from')
        t = getattr(self, 'to')
        direction = "east" if f.column < t.column else "west"
        from_ = self.get_pos(f.column, self.line)
        to_ = self.get_pos(t.column, self.line)
        self.text_above = self.data
        rev = "_reversed" if getattr(self, 'reversed', False) else ''
        return fr"""%% draw open window arrow
        \draw[pdl_script_action_arrow{rev}] ({self.node_name}.{direction}) to  {self.tikz_above()} ({to_});"""

    @property
    def height(self):
        return "4ex", "center"

class StartParty(PDLStep):
    yaml_tag = '!start-party'

    def tikz(self):
        pos = self.get_pos(self.party.column, self.line)
        text = self.party.name
        self.node_name = self.create_affecting_node_name((self.party, ))
        out = fr"""\node[name={self.node_name},pdl_start_party_box] at ({pos}) {{{text}}};"""
        return out
    
    def tikz_arrows(self):
        from_ = self.get_pos(self.party.column, self.line)
        to_ = self.get_pos(self.party.column, self.end.line)
        out = r"""\draw[pdl_lifeline] (%(from)s) -- (%(to)s);""" % {
            'from': from_,
            'to': to_,
        }
        return out

    def get_column(self):
        return self.party.column

    @property
    def height(self):
        return "5ex", "center"

class OpenWindowStartParty(StartParty):
    yaml_tag = '!open-window-start-party'

    def init(self):
        self.party = getattr(self, 'to')
    
    def tikz_arrows(self):
        f = getattr(self, 'from')
        t = getattr(self, 'to')
        direction = "east" if f.column > t.column else "west"
        from_ = self.get_pos(f.column, self.line)
        self.text_above = "open"
        out = fr"""%% draw open window arrow
        \draw[pdl_open_window_start_party_arrow] ({from_}) to  {self.tikz_above()} ({self.node_name}.{direction});"""
        out += super().tikz_arrows()
        return out
    

    @property
    def height(self):
        return "5ex", "center"

class EndParty(PDLStep):
    yaml_tag = '!end-party'

    def tikz(self):
        pos = self.get_pos(self.party.column, self.line)
        text = self.party.name
        node_name = self.create_affecting_node_name((self.party, ))
        out = fr"""\node[name={node_name},pdl_end_party_box] at ({pos}) {{{text}}};"""
        return out

    def get_column(self):
        return self.party.column

    @property
    def height(self):
        return "5ex", "center"

class Party(PDLObject):
    yaml_tag = '!Party'

    def add_affecting_node(self, node):
        if hasattr(self, '_affecting_nodes'):
            self._affecting_nodes.append(node)
        else:
            self._affecting_nodes = [node]

    @property
    def fit_string(self):
        if hasattr(self, '_affecting_nodes'):
            return self._affecting_nodes
        else:
            return ()

class Group(PDLObject):
    yaml_tag = '!Group'

    def tikz_desc(self):
        return f"""% drawing group {self.name}"""

    def tikz_groups(self, num_lines):
        columns_of_parties = {p.column:p for p in self.parties}
        first_column = min(columns_of_parties)
        first_party = columns_of_parties[first_column]
        last_column = max(columns_of_parties)
        last_party = columns_of_parties[last_column]
        fit_string = "fit=" + ''.join(f'({x})' for x in chain(
            [self.get_pos(first_column, 0)],
            first_party.fit_string,
            last_party.fit_string,
        ))
        gid = f"group{id(self)}"
        return fr"""\node[pdl_group_box,{fit_string}]({gid}) {{}}; \node[anchor=base,above=of {gid}.north,above=-2.5ex,anchor=base] {{{self.name}}};"""
    
        
class StyleDefault(yaml.YAMLObject):
    yaml_tag = "!style-default"
    style = r"""
    % basics
    every node/.style={font=\sffamily\tiny},
    pdl_lifeline/.style={draw=black!30},
    pdl_matrix_node/.style={},
    pdl_matrix_dummy_height/.style={},
    % default style used below
    line/.style={draw=blue},
    % groups
    pdl_group_box/.style={draw=black!50,dashed,rounded corners=1ex},
    pdl_group_title_placeholder/.style={},
    % start/end parties
    pdl_start_party_box/.style={fill=white,draw,rounded corners=0.3ex,anchor=center,inner sep=0.5ex,minimum height=1.7em,inner sep=1.5mm},
    pdl_end_party_box/.style={pdl_start_party_box,scale=0.7},
    % individual steps
    pdl_arrow_text/.style={},
    pdl_http_request/.style={-Latex,line},
    pdl_http_response/.style={-Latex[open],line},
    pdl_action/.style={fill=white,inner sep=1ex,minimum height=1.5em},
    pdl_open_window/.style={->,line},
    pdl_script_action_box/.style={pdl_action,anchor=center},
    pdl_script_action_arrow/.style={->,line},
    pdl_script_action_arrow_reversed/.style={<-,line},
    pdl_open_window_start_party_box/.style={pdl_start_party_box},
    pdl_open_window_start_party_arrow/.style={->,line},
    """

class StyleDebug(yaml.YAMLObject):
    yaml_tag = "!style-debug"

    style = """
    pdl_group_box/.style={fill=cyan!20},
    pdl_matrix_node/.style={draw=red},
    pdl_matrix_dummy_height/.style={draw=green},
    pdl_group_title_placeholder/.style={draw=pink}
    pdl_script_action_box/.style={draw=yellow},
    pdl_script_action_arrow/.style={->,draw=yellow},

    """
